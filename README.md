Pygdate
=======

Pygdate is a tool to see if installed gog.com games have updates available
without having to log in.

It goes through .desktop files in ~/.local/share/applications,
looks at installation path and tries to find game name, version and id from
gameinfo file located in path.

If ID isn't found in gameinfo, it tries to find game ID from gog website.

Pygdate depends on [requests library](https://3.python-requests.org/)

Usage
-----

run pygdate.py

By default pygdate only lists games that it considers to have updates
and finds game IDs and version strings from gog.

Options:

-v          shows errors

--all       lists all installed games

-g <game>   checks only for specific game, partial case insensitive name is             enough

Issues
-----

*Can not find IDs for all games

*All games do not have version numbers

*Versioning depends on developer so the values can be nonsense

*Some games have different version numbers in gog API and when installed
