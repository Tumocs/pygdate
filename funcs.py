"""helper functions for pygdate"""
import requests
import re

def usage():
    """prints the usage of program"""
    print("-a or --all for printing all found games")
    print("-v or --verbose for verbose output")
    print("-h or --help for this help")
    print("-g <game> or --game=<game> for only checking a specific game")


def find_id_online(name, print_all, version, verbose):
    """find gameID the hard way"""
    parsed_name = name.replace(":", "_")\
            .replace(" ", "_").lower()
    request = requests.get('https://gog.com/game/'\
            +parsed_name)
    findstr = "cardProductId: \""
    index = request.text.find(findstr)
    if index == -1:
        if print_all:
            print(name+"\n"+version+"\n")
        if verbose:
            print("Coudln't find ID for "\
                    +name+"\n")
        return index

    return request.text[index+16 : index+26]

def request_game(game_id, name, version, print_all, verbose):
    """get game info from gog"""
    request = requests.get(\
        'https://api.gog.com/products/'\
        +game_id+'?expand=downloads')
    for entry in request.json()['downloads']\
            ['installers']:
        if 'linux' in entry['os'] and\
                'en' in entry['language']:
            try:
                if version != entry['version']\
                        or print_all:
                    print(name+"\n"+version\
                            +"->"+entry['version']+"\n")
            except TypeError:
                if print_all:
                    print(name)
                    print(version+"\n")
                if verbose:
                    print(name)
                    print("Couldn't find version info\n")

def get_game(game, print_all, verbose, find_one, find_game):
    """parse games in folder"""
    game_list = []
    id_found = False
    new = False
    name = None
    version = None
    game_id = None
    one_found = False
    for i, gi_line in enumerate(game):
        if i == 0:
            name = gi_line.replace("\n", "")
            if find_one:
                if re.search(find_game, name, re.IGNORECASE):
                    print("did you mean " + name + "? [Y/n]")
                    check = input()
                    if check in ("Y", "y", ""):
                        one_found = True
            if name not in game_list:
                game_list.append(name)
                new = True
        if i == 1:
            version = gi_line.replace("\n", "")
        if i == 4:
            game_id = gi_line.replace("\n", "")
            id_found = True
    if (find_one and one_found) or not find_one:
        if new and not id_found:
            game_id = find_id_online(name, print_all, version, verbose)
            if game_id != -1:
                id_found = True
        if new and id_found:
            request_game(game_id, name, version, print_all, verbose)

    return one_found
