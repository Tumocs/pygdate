#!/usr/bin/python3
"""Sees if gog has any updates available"""
import os
import re
import getopt
import sys
import fnmatch
import requests
import funcs

def main():
    """main function"""

    search_path = os.path.expanduser('~/.local/share/applications/')
    search_line = "Path="

    verbose = False
    print_all = False
    find_one = False
    find_game = ""

    try:
        opts, args = getopt.getopt(sys.argv[1:], "havg:", ["help", "all", "verbose", "game="])
    except getopt.GetoptError as err:
        print(err)
        funcs.usage()
        sys.exit(2)

    for option, argument in opts:
        if option in ("-v", "--verbose"):
            verbose = True

        elif option in ("-a", "--all"):
            print_all = True

        elif option in ("-h", "--help"):
            funcs.usage()
            sys.exit()

        elif option in ("-g", "--game"):
            find_one = True
            find_game = argument

        #else:
        #    assert False, "unhandled option"

    for file in os.listdir(search_path):
        if fnmatch.fnmatch(file, 'gog_*'):
            with open(search_path+file, 'r') as f:
                for line in f:
                    if re.match(search_line, line):
                        npath = line.replace(search_line, "")
                        npath = npath.replace("\n", "/")
                        if os.path.isfile(npath+'gameinfo'):
                            with open(npath+'gameinfo', 'r') as g:
                                if funcs.get_game(g, print_all, verbose, find_one, find_game):
                                    sys.exit()

                        else:
                            print("gameinfo in " + npath + " not found\n"+\
                                    "you might have danglig .desktop file in you system")
                            print("Offending file is " + search_path + file + "\n")

if __name__ == "__main__":
    main()
